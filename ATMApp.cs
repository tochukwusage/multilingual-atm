﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultilingualATM
{
    public static class ATMApp
    {
        public static void Run()
        {
            var menu = new StringBuilder();
            menu.Append("Hello, welcome to XYZ bank \n");
            menu.AppendLine("Please insert your card: ");
            Console.WriteLine(menu.ToString());
            var cardNumber = Console.ReadLine();
            var user = new User();

            Console.WriteLine("Good day {0}. Kindly choose your language: \n 1. English \n 2. Igbo \n 3. Pidgin", user.fullName); // do date time thingy for greeting
            var language = Console.ReadLine();
            if (language != "1" && language != "2" && language != "3")
            {
                Console.WriteLine("INVALID LANGUAGE SELECTION!!!");
            }
            var selectedLanguage = LanguageSelection(language);

            
            if (language == "1")
            {
                Translator.WelcomeUserEnglish(user); // I will call the method here
                var pin = Console.ReadLine();
                while (pin != user.ATMPin)
                {
                    Translator.WrongPinEnglish();
                    pin = Console.ReadLine();
                }
                
                {
                    Translator.ChooseActionEnglish();
                    var chooseAction = Console.ReadLine();

                    while (chooseAction != "1" && chooseAction!= "2" && chooseAction != "3")
                    {
                        Translator.InvalidChoiceForActionEnglish();
                        Translator.ChooseActionEnglish();
                        chooseAction = Console.ReadLine();
                    }

                    if (chooseAction == "1")
                    {
                        Translator.DisplayAccountBalanceEnglish();
                    }
                }
                
            }
            if (language == "2")
            {
                Translator.WelcomeUserIgbo(user);
            }
            if (language == "3")
            {
                Translator.WelcomeUserPidgin(user);
            }

            
        }

        public static Language LanguageSelection(string language)
        {
            switch (language)
            {
                case "1":
                    return Language.English;
                case "2":
                    return Language.Igbo;
                case "3":
                    return Language.Pidgin;
                default:
                    return Language.SelectLanguage;
            }
            //return Console.WriteLine("Hello");
        }
        public static ChooseWhatYouWantToDo ChooseAction(string chooseAction)
        {
            switch (chooseAction)
            {
                case "1":
                    return ChooseWhatYouWantToDo.CheckBalance;
                case "2":
                    return ChooseWhatYouWantToDo.DepositCash;
                case "3":
                    return ChooseWhatYouWantToDo.OpenAccount;
                case "4":
                    return ChooseWhatYouWantToDo.WithdrawCash;
                default:
                    return ChooseWhatYouWantToDo.ChooseAction;
            }
        }
    }
    
}

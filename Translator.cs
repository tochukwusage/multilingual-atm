﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultilingualATM
{
    public class Translator
    {
        public static void WelcomeUserEnglish(User person)
        {
            Console.WriteLine("Welcome {0}. Please input your pin", person.fullName);
        }
        public static void WelcomeUserIgbo(User person)
        {
            Console.WriteLine("Ndeewo, onyeoma {0}. Biko Tinye piini gi: ", person.fullName);
        }
        public static void WelcomeUserPidgin(User person)
        {
            Console.WriteLine("{0}, Morn sah!!!. Abeg put your pin", person.fullName);
        }


        public static void WrongPinEnglish()
        {
            Console.WriteLine("Invalid pin! Please try again.");
        }
        public static void WrongPinIgbo()
        {
            Console.WriteLine("Akara gi aburo eziokwu");
        }
        public static void WrongPinPidgin()
        {
            Console.WriteLine("This your pin no pure o!");
        }


        public static void ChooseActionEnglish()
        {
            Console.WriteLine("Kindly select what you want to do:\n1. Check Account Balance\n2. Withdraw Cash\n3. Deposit Cash\n4. Open Account");
        }
        public static void ChooseActionIgbo()
        {
            Console.WriteLine("Biko kedu ihe i choro ime?");
        }
        public static void ChooseActionPidgin()
        {
            Console.WriteLine("Abeg choose the one wey you wan do: ");
        }


        public static void InvalidChoiceForActionEnglish()
        {
            Console.WriteLine("Invalid selection! Kindly choose another action");
        }
        public static void InvalidChoiceForActionIgbo()
        {
            Console.WriteLine("Ihe i hotara adighi mma! Biko tinye ozo");
        }
        public static void InvalidChoiceForActionPidgin()
        {
            Console.WriteLine("Be like say you wan crash our system. Abegii choose another thing");
        }

        
        public static void DisplayAccountBalanceEnglish()
        {
            //ATMApp.Run.user
            User user = new User();
            Console.WriteLine("Your account balance is: {0}", user.amount);
        }
    }
}

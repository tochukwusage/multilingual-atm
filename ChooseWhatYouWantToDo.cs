﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace MultilingualATM
{
    public enum ChooseWhatYouWantToDo
    {
        ChooseAction,
        CheckBalance,
        WithdrawCash,
        DepositCash,
        OpenAccount,
    }
}
